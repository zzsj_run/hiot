# 一句一句搭环境_hiot_鸿蒙WiFiiot

#### 介绍
virtualbox-ubt-code-1.0 
编译环境
virtualbox  6.1.30
#### 软件架构
软件架构说明


#### 安装教程

1.  ubuntu 镜像下载  https://releases.ubuntu-mate.org/focal/amd64/ubuntu-mate-20.04.1-desktop-amd64-gpd-micropc.iso
2.  code-1.0.tar.gz   gcc_riscv32-linux-7.3.0.tar.gz  llvm-linux-9.0.0-36191.tar  华为官网
3.  history.txt 是 安装命令集合
4.  1.0 源码 https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-1-0.md  选全量
#### 使用说明

1. sudo wget https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz
2.  sudo wget https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar
3.  sudo wget https://repo.huaweicloud.com/harmonyos/compiler/gn/1523/linux/gn.1523.tar
4.  https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/linux/llvm-linux-9.0.0-36191.tar

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
